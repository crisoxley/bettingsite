﻿using System.Collections.Generic;
using System.Linq;
using BettingSite.Entities.Enums;
using BettingSite.Entities.Models;
using BettingSite.Service;
using Moq;
using NUnit.Framework;

namespace BettingSite.Tests
{
    // Need to be able to see the odds of the horses
    // Need to be able to see the potential return for my bet

    [TestFixture]
    public class HorseyTests
    {
        private readonly Mock<HorseService> mockHorseService = new Mock<HorseService>();
        private readonly Mock<BetService> mockBetService = new Mock<BetService>();

        [Test]
        public void GetHorsesShouldDisplayListOfHorsesInSpecifiedRace()
        {
            // Arrange
            const int raceId = 2;

            var horseList = HorseList();

            // Act
            var result = mockHorseService.Object.GetHorses(raceId, horseList).ToList();

            // Assert
            Assert.IsInstanceOf(typeof(List<Horse>), result, "Not a List of horses");
            Assert.IsTrue(result.All(x => x.RaceId == raceId), "All values are not the same as the race Id");
            Assert.AreEqual(7, result[0].Id, "Incorrect PK Id");
        }

        [Test]
        public void AddHorseToBetSlipShouldReturnListOfSelectedHorses()
        {
            // Arrange
            const int raceId = 2;

            var horseList = HorseList(raceId).ToList();
            var betSlip = new BettingSlip();

            // Act
            Assert.IsNull(betSlip.Id, "betSlip.Id != null");

            for (var i = 0; i < 3; i++)
            {
                var horseId = horseList.Single(x => x.Id == i + 7).Id;
                var bet = new Bet { HorseId = horseId, RaceId = raceId, BetType = BetType.Win };

                betSlip = mockBetService.Object.AddHorseToBetSlip(betSlip, bet);

                Assert.AreEqual(i + 1, betSlip.Bets.Count, "Incorrect amount of bets in slip");
            }

            // Assert
            Assert.IsInstanceOf(typeof(BettingSlip), betSlip, "not a betslip type");
            Assert.AreEqual(3, betSlip.Bets.Count, "horseId count is not correct");
        }

        [Test]
        public void ReturnedHorseListShouldDisplayIndividualOdds()
        {
        }

        private static IEnumerable<Horse> HorseList(int? raceId = null)
        {
            var horseList = new List<Horse>
            {
                new Horse {Id = 1, RaceId = 1},
                new Horse {Id = 2, RaceId = 1},
                new Horse {Id = 3, RaceId = 1},
                new Horse {Id = 4, RaceId = 1},
                new Horse {Id = 5, RaceId = 1},
                new Horse {Id = 6, RaceId = 1},
                new Horse {Id = 7, RaceId = 2},
                new Horse {Id = 8, RaceId = 2},
                new Horse {Id = 9, RaceId = 2},
                new Horse {Id = 10, RaceId = 2},
                new Horse {Id = 11, RaceId = 2},
                new Horse {Id = 12, RaceId = 2},
                new Horse {Id = 13, RaceId = 3},
                new Horse {Id = 14, RaceId = 3},
                new Horse {Id = 15, RaceId = 3},
                new Horse {Id = 16, RaceId = 3},
                new Horse {Id = 17, RaceId = 3},
                new Horse {Id = 18, RaceId = 3},
                new Horse {Id = 19, RaceId = 4},
                new Horse {Id = 20, RaceId = 4},
                new Horse {Id = 21, RaceId = 4},
                new Horse {Id = 22, RaceId = 4},
                new Horse {Id = 23, RaceId = 4},
                new Horse {Id = 24, RaceId = 4}
            };

            return raceId.HasValue ? horseList.Where(x => x.RaceId == raceId).ToList() : horseList;
        }
    }
}
