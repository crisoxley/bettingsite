using System.Collections.Generic;
using System.Linq;
using BettingSite.Entities.Models;
using BettingSite.Service.Interfaces;

namespace BettingSite.Service
{
    public class HorseService : IHorseService
    {
        public IEnumerable<Horse> GetHorses(int raceId, IEnumerable<Horse> horseList)
        {
            return horseList.Where(x => x.RaceId == raceId).ToList();
        }
    }
}