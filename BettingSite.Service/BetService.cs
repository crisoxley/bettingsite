using BettingSite.Entities.Models;
using BettingSite.Service.Interfaces;

namespace BettingSite.Service
{
    public class BetService : IBetService
    {
        public BettingSlip AddHorseToBetSlip(BettingSlip betSlip, Bet bet)
        {
            if (betSlip.Id == null)
            {
                betSlip.Id = 1;
            }

            betSlip.Bets.Add(bet);

            return betSlip;
        }
    }
}