using BettingSite.Entities.Models;

namespace BettingSite.Service.Interfaces
{
    public interface IBetService
    {
        BettingSlip AddHorseToBetSlip(BettingSlip betSlip, Bet bet);
    }
}
