﻿using System.Collections.Generic;
using BettingSite.Entities.Models;

namespace BettingSite.Service.Interfaces
{
    public interface IHorseService
    {
        IEnumerable<Horse> GetHorses(int raceId, IEnumerable<Horse> horseList);
    }
}