﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BettingSite.Web.Startup))]
namespace BettingSite.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
