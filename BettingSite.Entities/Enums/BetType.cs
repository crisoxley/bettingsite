namespace BettingSite.Entities.Enums
{
    public enum BetType
    {
        Win = 10,
        EachWay = 20
    }
}
