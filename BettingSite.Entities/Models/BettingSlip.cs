﻿using System.Collections.Generic;

namespace BettingSite.Entities.Models
{
    public class BettingSlip
    {
        public BettingSlip()
        {
            Bets = new List<Bet>();
        }

        public int? Id { get; set; }

        public IList<Bet> Bets { get; set; }
    }
}