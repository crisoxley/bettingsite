using BettingSite.Entities.Enums;

namespace BettingSite.Entities.Models
{
    public class Bet
    {
        public int Id { get; set; }

        public BetType BetType { get; set; }

        public int HorseId { get; set; }

        public int RaceId { get; set; }
    }
}
