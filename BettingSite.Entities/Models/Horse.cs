namespace BettingSite.Entities.Models
{
    public class Horse
    {
        public int Id { get; set; }

        public int RaceId { get; set; }
    }
}